#include "A7.h"

// vector<string> *get_lines(string input, string delimiter)
// {
//     size_t pos = 0;
//     std::string token;
//     vector<string> *res = new vector<string>();
//     while ((pos = input.find(delimiter)) != std::string::npos)
//     {
//         token = input.substr(0, pos);
//         res->push_back(token);
//         // std::cout << token << std::endl;
//         input.erase(0, pos + delimiter.length());
//     }
//     return res;
// }

unsigned long a7_millis(void)
{
    return mgos_uptime() * 1000;
}

bool invalidChar(uint8_t c)
{
    return !(c < 128);
}
void stripUnicode(string &str)
{
    str.erase(remove_if(str.begin(), str.end(), invalidChar), str.end());
}

bool string_contains(string total, string what)
{
    return (total.find(what) != std::string::npos);
}
//int entry = 0;

void serial_dispatcher(int uartn, void *arg)
{
    Ai_A7 *modem = (Ai_A7 *)arg;
    int rx_pending;
    if ((rx_pending = mgos_uart_read_avail(uartn)) > 0)
    {
        char tempb[rx_pending + 1];
        memset(tempb, 0, sizeof(tempb));
        int rxed = mgos_uart_read(uartn, tempb, rx_pending + 1);
        if (modem->cmd_in_progress)
        {
            if (modem->cmd_cb)
            {
                bool found_reply = false;
                if (modem->cmd_has_replies)
                {
                    //LOG(LL_INFO, ("########### entry %d, rxed %d", entry++, rxed));
                    //s->write(tempb, rxed + 1);
                    string rxed_s(tempb);
                    stripUnicode(rxed_s);
                    *modem->cmd_buffer << rxed_s;
                    stringstream *s = new stringstream();
                    s->str("");
                    *s << modem->cmd_buffer->str();
                    char line[512];
                    //LOG(LL_INFO, ("#### cr 1"));
                    do
                    {
                        //LOG(LL_INFO, ("#### cr 2"));
                        s->getline(line, 512);
                        if (s->fail())
                        {
                            break;
                        }
                        int ln = strlen(line);
                        if (ln >= 1 && line[ln - 1] == '\r')
                        {
                            line[ln - 1] = '\0';
                        }
                        // if (ln > 1)
                        //     LOG(LL_INFO, ("#################  Line [%s]", line));
                        //LOG(LL_INFO, ("#### cr 3"));
                        string line_s(line);
                        if (line_s.length() > 0)
                        {
                            for (int i = 0; i < modem->cmd_replies->size(); i++)
                            {
                                // LOG(LL_INFO, ("#################  CMP [%s] ==? [%s]", line_s.c_str(), modem->cmd_replies->at(i).c_str()));
                                if (line_s == modem->cmd_replies->at(i) || string_contains(line_s, modem->cmd_replies->at(i)))
                                {
                                    //LOG(LL_INFO, ("#### cr 4"));
                                    bool ret = modem->cmd_cb(true, tempb, rxed, modem->ud);
                                    //LOG(LL_INFO, ("#### cr 5"));
                                    if (ret)
                                    { // fulfilled
                                        modem->clear_cmd();
                                    }
                                    found_reply = true;
                                    // LOG(LL_INFO, ("#################  Reply match!"));
                                    break;
                                }
                            }
                        }
                        //LOG(LL_INFO, ("#### cr 6"));
                    } while (!s->fail() && !found_reply);
                    delete s;
                    // if (!found_reply)
                    // {
                    //     LOG(LL_INFO, ("=------------------  NO Reply match! %d", modem->cmd_retry_cnt));
                    //     if (modem->cmd_retry_cnt)
                    //     {
                    //         modem->cmd_retry_cnt--;
                    //         mgos_uart_printf(modem->uartn, "%s\r\n", modem->last_cmd.c_str());
                    //         modem->cmd_entry = millis();
                    //     }
                    //     else
                    //     {
                    //         if (modem->cmd_cb)
                    //         {
                    //             bool ret = modem->cmd_cb(false, "no_reply", sizeof("no_reply"), modem->ud);
                    //             if (ret)
                    //             { // fulfilled
                    //                 modem->clear_cmd();
                    //             }
                    //         }
                    //     }
                    // }
                }
                else
                {
                    string rxed_s(tempb);
                    stripUnicode(rxed_s);
                    *modem->cmd_buffer << rxed_s;
                    string total = modem->cmd_buffer->str();
                    bool ret = modem->cmd_cb(true, (char *)total.c_str(), total.length(), modem->ud);
                    if (ret)
                    { // fulfilled
                        modem->clear_cmd();
                    }
                }
            }
            else
            {
                modem->clear_cmd();
            }
        }
        else if (modem->idle_cb)
        {
            modem->idle_cb(true, tempb, rxed, modem->ud);
        }
    }
}

void ticker(void *arg)
{
    Ai_A7 *modem = (Ai_A7 *)arg;
    if (modem->cmd_in_progress)
    {

        unsigned long now = a7_millis();
        // LOG(LL_INFO, ("#### cr 9921 %lu", now - modem->cmd_entry));
        if (now - modem->cmd_entry > modem->cmd_timeout)
        {
            // LOG(LL_INFO, ("#### cr 99"));
            if (modem->cmd_retry_cnt)
            {
                // LOG(LL_INFO, ("#### cr 76"));
                modem->cmd_retry_cnt--;
                modem->cmd_buffer->clear();
                modem->cmd_buffer->str("");
                mgos_uart_printf(modem->uartn, "%s\r\n", modem->last_cmd.c_str());
                modem->cmd_entry = now;
            }
            else
            {
                // LOG(LL_INFO, ("#### cr 833"));
                if (modem->cmd_cb)
                {
                    // LOG(LL_INFO, ("#### cr 67"));
                    modem->cmd_cb(false, (char *)"timeout", sizeof("timeout"), modem->ud);
                    // LOG(LL_INFO, ("#### cr 68"));
                    //if (ret)
                    //{ // fulfilled
                    modem->clear_cmd();
                    //}
                }
            }
        }
    }
}

void Ai_A7::cmd(string cmd, int retries, int timeout, my_cb_type cb, void *userdata, bool newline)
{
    if (cmd_in_progress)
        return;
    cmd_retry_cnt = retries;
    cmd_cb = cb;
    cmd_in_progress = true;
    ud = userdata;
    last_cmd = cmd;
    cmd_has_replies = false;
    cmd_buffer = new stringstream();
    cmd_buffer->clear();
    cmd_buffer->str("");
    cmd_entry = a7_millis();
    cmd_timeout = timeout;
    if (newline)
        mgos_uart_printf(uartn, "%s\r\n", cmd.c_str());
    else
        mgos_uart_printf(uartn, "%s", cmd.c_str());
}

void Ai_A7::cmd_with_reply(string cmd, std::vector<std::string> replies, int retries, int timeout, my_cb_type cb, void *userdata)
{
    if (cmd_in_progress)
        return;
    if (replies.empty())
        return;

    cmd_retry_cnt = retries;
    cmd_cb = cb;
    cmd_in_progress = true;
    ud = userdata;
    last_cmd = cmd;
    cmd_buffer = new stringstream();
    cmd_buffer->clear();
    cmd_buffer->str("");
    cmd_entry = a7_millis();
    cmd_timeout = timeout;
    cmd_replies = new std::vector<std::string>();
    cmd_replies->assign(replies.begin(), replies.end());
    cmd_has_replies = true;
    mgos_uart_printf(uartn, "%s\r\n", cmd.c_str());
}

void Ai_A7::cmd_no_reply(string cmd)
{
    if (cmd_in_progress)
        return;
    mgos_uart_printf(uartn, "%s\r\n", cmd.c_str());
}

void Ai_A7::clear_cmd()
{
    cmd_cb = NULL;
    cmd_in_progress = false;
    ud = NULL;
    last_cmd = "";
    cmd_has_replies = false;
    if (cmd_replies)
    {
        cmd_replies->clear();
        delete cmd_replies;
        cmd_replies = NULL;
    }
    if (cmd_buffer)
    {
        cmd_buffer->clear();
        delete cmd_buffer;
        cmd_buffer = NULL;
    }
}

Ai_A7::Ai_A7()
{
    // cmd_replies = {""};
}
Ai_A7::~Ai_A7()
{
    // if (BoardSerial != NULL)
    // {
    //   delete BoardSerial;
    //   BoardSerial = NULL;
    // }
}
Ai_A7::Ai_A7(int uartn, int RX, int TX, int Power, int Reset, long baudrate)
{
    Ai_A7();
    setup(uartn, RX, TX, Power, Reset);
}
void Ai_A7::setup(int uartn, int RX, int TX, int Power, int Reset, long baudrate)
{
    this->uartn = uartn;
    PIN_Power = Power;
    PIN_Reset = Reset;
    PIN_RX = RX;
    PIN_TX = TX;

    struct mgos_uart_config ucfg;
    mgos_uart_config_set_defaults(uartn, &ucfg);

    ucfg.baud_rate = baudrate;
    ucfg.rx_buf_size = 512;
    ucfg.tx_buf_size = 512;
    ucfg.dev.rx_gpio = PIN_RX;
    ucfg.dev.tx_gpio = PIN_TX;

    if (!mgos_uart_configure(uartn, &ucfg))
    {
        LOG(LL_ERROR, ("Failed to configure UART%d", uartn));
    }

    mgos_uart_set_rx_enabled(2, true);
    // //board=t;
    // if(BoardSerial!=NULL){
    //   delete BoardSerial;
    //   BoardSerial=NULL;
    // }
    // BoardSerial=new SoftwareSerial(RX,TX);

    cmd_replies = NULL;
    cmd_buffer = NULL;
    idle_cb = NULL;
    cmd_cb = NULL;
    clear_cmd();
    mgos_uart_set_dispatcher(uartn, serial_dispatcher, this);
    mgos_set_timer(100, MGOS_TIMER_REPEAT, ticker, this);
    // mb_states[uartn].transaction_timeout = resp_timeout;
}

void Ai_A7::begin(long baudrate, bool resetp)
{
    struct mgos_uart_config ucfg;
    mgos_uart_config_get(uartn, &ucfg);
    ucfg.baud_rate = baudrate;
    mgos_uart_configure(uartn, &ucfg);

    // // BoardSerial->begin(baudrate);

    if (resetp)
    {
        reset();
        //     _start();
        //     Operator();
        //     //SetAPN("TM");
    }
    power_on();
}

void Ai_A7::change_baudrate(long baudrate)
{
    struct mgos_uart_config ucfg;
    mgos_uart_config_get(uartn, &ucfg);
    ucfg.baud_rate = baudrate;
    mgos_uart_configure(uartn, &ucfg);
}

void Ai_A7::power_on()
{
    if (PIN_Power > 0)
    {
        mgos_gpio_setup_output(PIN_Power, 0);
    }
    if (PIN_Reset > 0)
    {
        mgos_gpio_setup_output(PIN_Reset, 0);
    }
    if (PIN_Power > 0)
    {
        mgos_gpio_setup_output(PIN_Power, 1);
    }
    mgos_msleep(2000);
    if (PIN_Power > 0)
    {
        mgos_gpio_setup_output(PIN_Power, 0);
    }
}

bool Ai_A7::power_on_non_blocking(bool first)
{
    if (first)
    {
        power_on_millis = a7_millis();
        if (PIN_Power > 0)
        {
            mgos_gpio_setup_output(PIN_Power, 0);
        }
        if (PIN_Reset > 0)
        {
            mgos_gpio_setup_output(PIN_Reset, 0);
        }
        if (PIN_Power > 0)
        {
            mgos_gpio_setup_output(PIN_Power, 1);
        }
    }
    else if (a7_millis() - power_on_millis > 2000)
    {
        //mgos_msleep(2000);
        if (PIN_Power > 0)
        {
            mgos_gpio_setup_output(PIN_Power, 0);
        }
        if (a7_millis() - power_on_millis > 2200)
        {
            return true;
        }
    }
    return false;
}

void Ai_A7::reset()
{
    if (PIN_Reset > 0)
    {
        mgos_gpio_setup_output(PIN_Reset, 1);
        mgos_msleep(1000);
        mgos_gpio_setup_output(PIN_Reset, 0);
    }
    mgos_msleep(200);
}

bool Ai_A7::reset_non_blocking(bool first)
{
    if (first)
    {
        reset_millis = a7_millis();
        if (PIN_Reset > 0)
        {
            mgos_gpio_setup_output(PIN_Reset, 1);
        }
    }
    else if (a7_millis() - reset_millis > 1000)
    {
        if (PIN_Reset > 0)
        {
            mgos_gpio_setup_output(PIN_Reset, 0);
        }
        if (a7_millis() - reset_millis > 1200)
        {
            return true;
        }
    }
    return false;
    // if (PIN_Reset > 0)
    // {
    //     mgos_gpio_setup_output(PIN_Reset, 1);
    //     mgos_msleep(1000);
    //     mgos_gpio_setup_output(PIN_Reset, 0);
    // }
    // mgos_msleep(200);
}

string Ai_A7::signal_to_dbm(int signal)
{
    if (signal < 2 || signal > 30)
        return "Invalid dbm";
    stringstream res;
    res << (2 * (signal - 2) - 109) << " dbm";
    return res.str();
}
string Ai_A7::signal_to_level(int signal)
{
    if (signal < 2 || signal > 30)
        return "Invalid level";
    if (signal <= 9)
        return "Marginal level";
    if (signal <= 14)
        return "Ok level";
    if (signal <= 19)
        return "Good level";

    return "Excellent level";
}

string Ai_A7::creg_to_string(int creg)
{
    switch (creg)
    {
    case 0:
        return "not registered, not searching";
    case 1:
        return "registered, home network";
    case 2:
        return "not registered, searching";
    case 3:
        return "registration denied";
    case 4:
        return "unknown registration (no signal?)";
    case 5:
        return "registered, roaming";
    case 6:
        return "registered, only sms, home network";
    case 7:
        return "registered, only sms, roaming";
    case 8:
        return "emergency only mode";
    case 9:
        return "registered, CSFB not preferred, home network";
    case 10:
        return "registered, CSFB not preferred, roaming";
    }

    return "invalid registration status";
}

bool mgos_a7_gsm_init(void)
{
    return true;
}