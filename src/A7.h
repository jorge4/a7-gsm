#pragma once

#include "mgos.h"
#include "mgos_mongoose.h"
#include "mgos_gpio.h"
#include "mgos_uart.h"
#include <string>
#include <sstream>
#include <bits/stdc++.h>
#include <vector>

using namespace std;

typedef bool (*my_cb_type)(bool success, char *data, int len, void *userdata);

// vector<string> * get_lines(string input, string delimiter = "\r\n");
bool string_contains(string total, string what);

extern "C" {
    // extern unsigned long millis(void);
    bool mgos_a7_gsm_init(void);
}

// #define GPRS_Serial_RX 35
// #define GPRS_Serial_TX 33
// #define GPRS_Power_pin 32
// #define GPRS_Reset_pin 0


class Ai_A7{

private:
    // const int DBMS[5] = {-1, -1, -1, -109,-107,-105};
    unsigned long reset_millis;
    unsigned long power_on_millis;

public:
  int PIN_Power;
  int PIN_Reset;
  int PIN_RX;
  int PIN_TX;
  int uartn;

  stringstream input;
  bool cmd_in_progress;
  my_cb_type cmd_cb;
  my_cb_type idle_cb;
  void * ud;
  string last_cmd;
  unsigned long cmd_entry;
  unsigned long cmd_timeout;
  int cmd_retry_cnt;
  bool cmd_has_replies;
  std::vector<std::string> * cmd_replies;
  stringstream * cmd_buffer;

  bool debug_on=false;

public:
  Ai_A7();
  ~Ai_A7();
  Ai_A7(int uartn, int RX,int TX,int Power=0,int Reset=0, long baudrate=9600);

  // Get the ccid of the SIM card
//   String CCID();
  // pin set
  void setup(int uartn, int RX,int TX,int Power=0,int Reset=0, long baudrate=9600);
  /*
  Start the module
  Baudrate baud rate with module communication
  Reset Whether to call _start () to reset the module settings
  */
  void begin(long baudrate=9600,bool reset=true);
  // start the module, need to set the power pin to be effective
  void power_on();
  bool power_on_non_blocking(bool first);
   /*
  Reset the module, need to set the Reset pin to be effective
  After reset, call bigin to reinitialize the module
  */
  void reset();
  bool reset_non_blocking(bool first);

  void change_baudrate(long baudrate);


  void clear_cmd();

  void cmd(string cmd, int retries, int timeout, my_cb_type cb, void * userdata, bool newline = true);
  void cmd_with_reply(string cmd, std::vector<std::string> replies, int retries, int timeout, my_cb_type cb, void * userdata);
  void cmd_no_reply(string cmd);

  string signal_to_dbm(int signal);
  string signal_to_level(int signal);
  string creg_to_string(int creg);

  /*
  Into the serial port through the debug mode, you can directly through the serial port through the command to the module
  Used for debugging, no exit after entering
  */
//   void serial_debug();
//   // start debugging information output, output to the serial port
//   void debug(bool is_on=true);
//   // execute a command
//   byte cmd(String command, String response1, String response2, int timeOut=500, int repetitions=2);
//   byte waitFor(String response1, String response2, unsigned long timeOut);
//   String BoardRead();
//   // initialize the A6 module and prepare the at environment
//   bool _start();

//   bool Operator();

//   bool SetAPN(String APN);
  
//   // initialize the GPRS environment and start GPRS
//   bool GPRS_Start(String APN);
//   // Create a TCP link
//   bool TCP(String host,String port);
//   // Create a UDP link
//   bool UDP(String host,String port);
//   // After creating a link (TCP or UDP), send a piece of data
//   bool Send(String data);
//   // disconnect the link (TCP or UDP)
//   bool Close();
//   // send data at once, including the following actions: link, send, close
//   bool Send_once(String host,String port,String data);
//   // simply send an at command to the module without waiting for a return
//   void at(String cmd);
};